#include "Record.h"



Record::Record()
{
}

bool Record::openFile()
{
	this->file.open(this->fileName);

	if (file.is_open())
		return true;

	return false;
}

bool Record::closeFile()
{
	this->file.close();

	if (!file)
		return true;
	else
	{
		file.close();
		return true;
	}
}

void Record::setFileName(string name)
{
	this->fileName = name;
}

string Record::readLine()
{
	string line;

	getline(file, line);
	return line;
}

bool Record::writeLine(string str)
{
	file << str << endl;
	return true;
}

Record::~Record()
{
}

Record & operator<<(Record & record, string str)
{
	record.file << str;
	return record;
}

Record & operator >> (Record & record, string &str)
{
	getline(record.file, str);
	return record;
}
