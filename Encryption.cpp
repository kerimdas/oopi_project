#include "Encryption.h"



Encryption::Encryption() //Yapici fonksiyon olarak Encryption tanimlandi.
{
}

int Encryption::encrypt(int number)
{
	int newNumber;

	int firstValue = number / 1000;                // Sayinin ilk basamagi alindi.
	int secondValue = (number % 1000) / 100;      // Sayinin ikinci basamagi alindi.
	int thirdValue = (number % 100) / 10;        // Sayinin ucuncu basamagi alindi.
	int fourthValue = number % 10;              // Sayinin dorduncu basamagi alindi.

	// cout << a << "  " << b << "  " << c << "  " << d << endl; 
	// Replace each digit with the result of adding 7 to the digit and getting
	// the remainder after dividing the new value by 10. 

	firstValue = (firstValue + 7) % 10;        //Sayinin ilk basamagina 7 eklenip 10'a bolunur.
	secondValue = (secondValue + 7) % 10;     //Sayinin ikinci basamagina 7 eklenip 10'a bolunur.
	thirdValue = (thirdValue + 7) % 10;      //Sayinin ucuncu basamagina 7 eklenip 10'a bolunur.
	fourthValue = (fourthValue + 7) % 10;   //Sayinin dorduncu basamagina 7 eklenip 10'a bolunur.
	
	newNumber = (thirdValue * 1000 + fourthValue * 100 + firstValue * 10 + secondValue);

	return newNumber;
}

int Encryption::decrypt(int number)
{
	int newNumber;

	int firstValue = number / 1000;              // Sayinin ilk basamagi alindi.
	int secondValue = (number % 1000) / 100;    // Sayinin ikinci basamagi alindi.
	int thirdValue = (number % 100) / 10;      // Sayinin ucuncu basamagi alindi.
	int fourthValue = number % 10;            // Sayinin dorduncu basamagi alindi.

	// cout << a << "  " << b << "  " << c << "  " << d << endl; 
	// Replace each digit with the result of adding 7 to the digit and getting
	// the remainder after dividing the new value by 10. 

	firstValue = (firstValue + 3) % 10;      //Sayinin ilk basamagina 7 eklenip 10'a bolunur.
	secondValue = (secondValue + 3) % 10;    //Sayinin ikinci basamagina 7 eklenip 10'a bolunur.
	thirdValue = (thirdValue + 3) % 10;      //Sayinin ucuncu basamagina 7 eklenip 10'a bolunur.
	fourthValue = (fourthValue + 3) % 10;    //Sayinin dorduncu basamagina 7 eklenip 10'a bolunur.

	newNumber = (thirdValue * 1000 + fourthValue * 100 + firstValue * 10 + secondValue);

	return newNumber;
}


Encryption::~Encryption()  //Dectructure olarak Encrytion fonksiyonu tanimlandi.
{
}
