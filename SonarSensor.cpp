/**
 *	@file	:	SonarSensor.cpp
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Sonar mesafe sensoru icin veri tutma ve yonetimini saglar.
 **/
#include "SonarSensor.h"

SonarSensor::SonarSensor()
{
}
/**
 *	@brief	:	i. indeksine sahip sensorun mesafe bilgisini dondurur.
 *	@params	:	int index
 **/
float SonarSensor::getRange(int index)
{
	return ranges[index];

}
/**
 *	@brief	:	Mesafe degerlerinden maksimum olani dondurur.
 *	@params	:	int &index
 **/
float SonarSensor::getMax(int &index)
{
	float max = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] > max)
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}
/**
 *	@brief	:	Mesafe degerlerinden minimum olani dondurur.
 *	@params	:	int &index
 **/
float SonarSensor::getMin(int &index)
{
	float min = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] < min)
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}
/**
 *	@brief	:	Robota ait guncel sensor mesafe degerlerini, 'ranges' dizisine yukler.
 *	@params	:	float range[]
 **/
void SonarSensor::updateSensor(float range[])
{
	for (int i = 0; i < 16; i++)
	{
		ranges[i] = range[i];
	}
}
/**
 *	@brief	:	Indeksi verilen sensor degerini dondurur.
 *	@params	:	int i
 **/
float SonarSensor::operator[](int i)
{
	return ranges[i];
}
/**
 *	@brief	:	Indeksi verilen sensorun aci degerini dondurur.
 *	@params	:	int i
 **/
float SonarSensor::getAngle(int i)
{
	return ranges[i];
}

SonarSensor::~SonarSensor()
{
}
