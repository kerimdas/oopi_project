#include "PioneerRobotAPI.h"
#include "RobotControl.h"
#include <iostream>
using namespace std;

PioneerRobotAPI *robot;
float sonars[16];
float laserData[181];

void print() {
	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	cout << "Sonar ranges are [ ";
	robot->getSonarRange(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sonars[i] << " ";
	}
	cout << "]" << endl;
	cout << "Laser ranges are [ ";
	robot->getLaserRange(laserData);
	for (int i = 0; i < 181; i++) {
		cout << laserData[i] << " ";
	}
	cout << "]" << endl;
}

int main() {

	robot = new PioneerRobotAPI;

	RobotControl *robotControl;
	robotControl = new RobotControl(robot);

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	robotControl->print();

	robotControl->forward(1000);
	Sleep(1000);
	print();

	robotControl->turnLeft();
	Sleep(1000);
	print();

	robotControl->forward(1000);
	Sleep(1000);
	print();

	robotControl->backward(1000);
	Sleep(1000);
	print();

	robotControl->turnRight();
	Sleep(1000);
	print();

	//robotControl->safeMoveRobot(1000);
	//Sleep(1000);
	//print();

	cout << "Press any key to exit...";
	getchar();

	robot->disconnect();
	delete robot;
	return 0;

}