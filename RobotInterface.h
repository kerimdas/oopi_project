#pragma once

#include "Pose.h"

class RobotInterface
{
public:
	RobotInterface();
	~RobotInterface();

	virtual void turnLeft() = 0;
	virtual void turnRight() = 0;
	virtual void forward(float speed) = 0;
	virtual void print() = 0;
	virtual void backward(float speed) = 0;
	virtual Pose getPose() = 0;
	virtual void setPose(Pose* pose);
	virtual void stopTurn() = 0;
	virtual void stopMove() = 0;
	virtual void updateSensors() = 0;

private:
	Pose * position;
	int state;
};

