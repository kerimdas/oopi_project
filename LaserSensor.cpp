/**
 *	@file	:	LaserSensor.cpp
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Lazer mesafe sensoru icin veri tutma ve yonetimini saglar.
 **/

#include "LaserSensor.h"

LaserSensor::LaserSensor()
{
}

LaserSensor::~LaserSensor()
{
}
/**
 *	@brief	:	i. indeksine sahip sensorun mesafe bilgisini dondurur.
 *	@params	:	int index
 **/
float LaserSensor::getRange(int index)
{
    return ranges[index];
}
/**
 *	@brief	:	Robota ait guncel sensor mesafe degerlerini, 'ranges' dizisine yukler.
 *	@params	:	float range[]
 **/
void LaserSensor::updateSensor(float range[])
{
	for (int i = 0; i < 16; i++)
	{
		ranges[i] = range[i];
	}
}
/**
 *	@brief	:	Mesafe degerlerinden maksimum olani dondurur.
 *	@params	:	int &index
 **/
float LaserSensor::getMax(int &index)
{
	float max = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] > max)
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}
/**
 *	@brief	:	Mesafe degerlerinden minimum olani dondurur.
 *	@params	:	int &index
 **/
float LaserSensor::getMin(int &index)
{
	float min = ranges[0];
	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] < min)
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}
/**
 *	@brief	:	Indeksi verilen sensor degerini dondurur.
 *	@params	:	int i
 **/
float LaserSensor::operator[](int i)
{
	return ranges[i];
}
/**
 *	@brief	:	Indeksi verilen sensorun aci degerini dondurur.
 *	@params :	int i
 **/
float LaserSensor::getAngle(int i)
{
	return ranges[i];
}
/**
 *	@brief	:	startAngle ve endAngle acilari arasinda kalan mesafelerden
 *				en kucuk olaninin acisini angle uzerinde, mesafeyi ise 'return' ile dondurur.
 *	@params	:	float startAngle, float endAngle, float& angle
 **/
float LaserSensor::getClosestRange(float startAngle,float  endAngle, float & angle)
{
	float closest = ranges[(int)startAngle];

	for(int i = startAngle + 1; i < endAngle; i++)
	{
		if(closest > ranges[i])
		{
			closest = ranges[i];
			angle = i;
		}
	}
	return closest;
}
