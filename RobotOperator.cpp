#include "RobotOperator.h"



RobotOperator::RobotOperator(int number) //Yapici fonksiyon olarak RobotOpertor tanimlandi.
{
	accessCode = encryptCode(number);// Sifrelenmis olarak tutulan accessCode, encrytCode ile sifrelendirildi.
}

bool RobotOperator::checkAcessCode(int code)
{ 
	if (accessCode == code)//Girilen kodun, sifrelenmis olarak tutulan accessCode ile ayni olup olmadigini kontrol eder. 
		return true;
	return false;
}

void RobotOperator::print() //: Operatorun, adini soyadini ve erisim durumunu ekrana getirir. 
{
	cout << "Name Surname: " << name << " " << surname << " " << " State: " << accessState;
}

void RobotOperator::setName(string name)// Operatorun adini alir.
{
	this->name = name;
}

string RobotOperator::getName()
{
	return name;
}

void RobotOperator::setSurname(string surname)//Operatorun soyadini alir.
{
	this->surname = surname;
}

string RobotOperator::getSurname()
{
	return surname;
}

void RobotOperator::setAccessState()//Operatorun erisim durumunu alir.
{
	accessState = checkAcessCode(accessCode);
}


RobotOperator::~RobotOperator()//Dectructure olarak RobotOperator fonksiyonu tanimlandi.
{
} 

int RobotOperator::encryptCode(int number)//Encryption sinifinin fonksiyonu kullanilarak sifrelendirir.      
{
	return encrypt(number);
}

int RobotOperator::decryptCode(int number)// Encryption sinifinin fonksiyonu kullanilarak sifresini cozdurur.
{
	return decrypt(number);
}
