/**
 *	@file	:	RobotControl.cpp
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Robotun simulatordeki kontrolunu saglamak icin olusturulmustur.
 **/

#include "RobotControl.h"

RobotControl::RobotControl(PioneerRobotAPI* robot) {
	
}
/**
 *	@brief	:	Robotu simulatorde sola dondurmek icin yazilmistir.
 *
 **/
void RobotControl::turnLeft() {
	robotInterface->turnLeft();
	//robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}
/**
 *	@brief	:	Robotu simulatorde saga dondurmek icin yazilmistir.
 *
 **/
void RobotControl::turnRight() {
	robotInterface->turnRight();
	//robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}
/**
 *	@brief	:	Robotu simulatorde ileriye dogru ilerletmek icin yazilmistir.
 *	@params	:	float speed
 **/
void RobotControl::forward(float speed) {
	robotInterface->forward(speed);
	//robotAPI->moveRobot(speed);
}

/**
 *
 *	@brief	:	Robotun simulatorde guvenli bir sekilde hareket etmesini saglar.
 *	@params	:	float speed
 *
 **/
void RobotControl::safeMoveRobot(float speed) {
	
}
/**
 *	@brief	:	Konsola 'RobotControl is active...' ciktisi aldirir.
 *
 **/
void RobotControl::print() {
	robotInterface->print();
	//cout << "RobotControl is active..." << endl;
}
/**
 *	@brief	:	Robotu simulatorde geriye dogru ilerletmek icin yazilmistir.
 *	@params	:	float speed
 **/
void RobotControl::backward(float speed) {
	robotInterface->backward(speed);
	//robotAPI->moveRobot(-1 * speed);
}
/**
 *	@brief	:	Robotun pozisyonunu dondurur.
 *
 **/
Pose RobotControl::getPose() {
	return robotInterface->getPose();
}
/**
 *	@brief	:	Robotun pozisyonunu ayarlamak icin yazilmistir.
 *	@params	:	Pose pose
 **/
void RobotControl::setPose(Pose pose) {
	this->robotInterface->setPose = &pose;
}
/**
 *	@brief	:	Robotu simulatorde donmesini durdurmak icin yazilmistir.
 *
 **/
void RobotControl::stopTurn() {
	robotInterface->stopTurn();
	//robotAPI->stopRobot();
}
/**
 *	@brief	:	Robotu simulatorde durdurmak icin yazilmistir.
 *
 **/
void RobotControl::stopMove() {
	robotInterface->stopMove();
	//robotAPI->stopRobot();
}

bool RobotControl::addToPath() {
		
}

bool RobotControl::clearPath() {

}

bool RobotControl::recordPathToFile() {

}

bool RobotControl::openAccess(int _key) {
	
}

bool RobotControl::closeAccess(int _key) {

}