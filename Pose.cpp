/*
* @file Pose.cpp
* @Author Zuhal POLAT (zuhalpolat55@gmail.com)
* @date 14.12.2018
* @brief class Pose
*/

#include "Pose.h"


// Definition : Pose class' constructor without parameters
Pose::Pose()
{
	x = 0;
	y = 0;
	th = 0;
}


// Definition : Pose class' constructor with parameters
// Parameters : x, y, and th in integer type. x and y in mm, th in �
Pose::Pose(int x, int y, int th)
{
	setX(x);
	setY(y);
	setTh(th);
}

// Definition : Pose class' deconstructor
Pose::~Pose()
{
}

// Definition : The function retruns x
// Return : x in float type
float Pose::getX()
{
	return x;
}

// Definition : The function set x value
// Parameters : x in float type
void Pose::setX(float x)
{
	this->x = x;
}

// Definition : The function retruns y
// Return : y in float type
float Pose::getY()
{
	return y;
}

// Definition : The function set y value
// Parameters : y in float type
void Pose::setY(float y)
{
	this->y = y;
}

// Definition : The function retruns th
// Return : th in float type
float Pose::getTh()
{
	return th;
}

// Definition : The function set th value
// Parameters : th in float type
void Pose::setTh(float th)
{
	this->th = th;
}

// Definition : The function compares the parameter's x, y and th values with the class' x, y, and th values
// Parameters : other in const Pose& type
// Return : The function returns true if the values are equal with each other, if they aren't so returns false
bool Pose::operator==(const Pose & other)
{
	if (x == other.x && y == other.y && th == other.th)
		return true;
	else
		return false;
}

// Definition : The function is a operator+. Sum the other's x, y and th with class' x, y and th values. Assign the values to the temp and return it.
// Parameters : other in const Pose& type
// Return : The function returns in Pose type
Pose Pose::operator+(const Pose & other)
{
	Pose temp;

	temp.x = this->x + other.x;
	temp.y = this->y + other.y;
	temp.th = this->th + other.th;

	return temp;
}

// Definition : The function is a operator-. Subtract the other's x, y and th with class' x, y and th values. Assign the values to the temp and return it.
// Parameters : other in const Pose& type
// Return : The function returns in Pose type
Pose Pose::operator-(const Pose & other)
{
	Pose temp;

	temp.x = this->x - other.x;
	temp.y = this->y - other.y;
	temp.th = this->th - other.th;

	return temp;
}

// Definition : The function is a operator+=. Sum the other's x, y and th with class' x, y and th values.
// Parameters : other in const Pose& type
// Return : The function not actually return just assign the values
Pose& Pose::operator+=(const Pose & other)
{
	x = x + other.x;
	y = y + other.y;
	th = th + other.th;

	return *this;
}

// Definition : The function is a operator-=. Subtract the other's x, y and th with class' x, y and th values.
// Parameters : other in const Pose& type
// Return : The function not actually return just assign the values
Pose& Pose::operator-=(const Pose & other)
{
	x = x - other.x;
	y = y - other.y;
	th = th - other.th;

	return *this;
}

// Definition : The function is a operator<. Compare other's x, y and th with class' x, y and th values.
// Parameters : other in const Pose& type
// Return : The function return true if the all the values of the other's are bigger then class' values
bool Pose::operator<(const Pose & other)
{
	if (x < other.x && y < other.y && th < other.th)
		return true;
	else
		return false;
}

// Definition : The function get the Pose x, y and th values and assigns them to the parameters
// Parameters : _x, _y, _th in float& type
void Pose::getPose(float& _x, float& _y, float& _th)
{
	_x = x;
	_y = y;
	_th = th;
}

// Definition : The function set the  x, y and th values as the parameters values
// Parameters : _x, _y, _th in float type
void Pose::setPose(float _x, float _y, float _th)
{
	setX(_x);
	setY(_y);
	setTh(_th);
}

// Definition : The function finds the distance betwwen two points. The formula : ((x1- x2)^2 + (y1 - y2)^2)^(1/2)
// Parameters : pos in Pose type
// Return : The function returns the distance between the two points in float type
float Pose::findDistanceTo(Pose pos)
{
	float xValue = pow((this->getX() - pos.getX()), 2);
	float yValue = pow((this->getY() - pos.getY()), 2);

	float result = sqrt(xValue + yValue);

	return result;
}

// Definition : The function find the angle between pos' th value and Pose th value. For it we should subtract the values from each other.
//				If the difference is negative, we subtract it from 360
// Parameters : pos in Pose type
// Return : The function returns the angle between the th values in float type
float Pose::findAngleTo(Pose pos)
{
	float dif = th - pos.th;

	if (dif < 0)
		return (360 + dif);
	else
		return dif;
}