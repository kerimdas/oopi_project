/**
 *	@file	:	PioneerRobotInterface.cpp
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Robotun simulatordeki kontrolunu saglamak icin olusturulmustur.
 **/

#include "PioneerRobotInterface.h"

PioneerRobotInterface::PioneerRobotInterface(PioneerRobotAPI* robot) {
	robotAPI = robot;
}
/**
 *	@brief	:	Robotu simulatorde sola dondurmek icin yazilmistir.
 *
 **/
void PioneerRobotInterface::turnLeft() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}
/**
 *	@brief	:	Robotu simulatorde saga dondurmek icin yazilmistir.
 *
 **/
void PioneerRobotInterface::turnRight() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}
/**
 *	@brief	:	Robotu simulatorde ileriye dogru ilerletmek icin yazilmistir.
 *	@params	:	float speed
 **/
void PioneerRobotInterface::forward(float speed) {
	robotAPI->moveRobot(speed);
}

/**
 *
 *	@brief	:	Robotun simulatorde guvenli bir sekilde hareket etmesini saglar.
 *	@params	:	float speed
 *
 **/
void PioneerRobotInterface::safeMoveRobot(float speed) {

}
/**
 *	@brief	:	Konsola 'RobotControl is active...' ciktisi aldirir.
 *
 **/
void PioneerRobotInterface::print() {
	std::cout << "PioneerRobotInterface is active..." << std::endl;
}
/**
 *	@brief	:	Robotu simulatorde geriye dogru ilerletmek icin yazilmistir.
 *	@params	:	float speed
 **/
void PioneerRobotInterface::backward(float speed) {
	robotAPI->moveRobot(-1 * speed);
}
/**
 *	@brief	:	Robotun pozisyonunu dondurur.
 *
 **/
Pose PioneerRobotInterface::getPose() {
	return this->getPose();
}
/**
 *	@brief	:	Robotun pozisyonunu ayarlamak icin yazilmistir.
 *	@params	:	Pose pose
 **/
void PioneerRobotInterface::setPose(Pose pose) {
	float x = robotAPI->getX;
	float y = robotAPI->getY;
	float th = robotAPI->getTh;
	robotAPI->setPose(x, y, th);
}
/**
 *	@brief	:	Robotu simulatorde donmesini durdurmak icin yazilmistir.
 *
 **/
void PioneerRobotInterface::stopTurn() {
	robotAPI->stopRobot();
}
/**
 *	@brief	:	Robotu simulatorde durdurmak icin yazilmistir.
 *
 **/
void PioneerRobotInterface::stopMove() {
	robotAPI->stopRobot();
}

void PioneerRobotInterface::updateSensors() {

}