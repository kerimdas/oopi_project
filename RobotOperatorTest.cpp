#include <iostream>
#include "RobotOperator.h"

int main()
{
	RobotOperator robot(1254);								//Created a object
	bool result;
	int number = 2189, number2;

	result = robot.checkAcessCode(number);					//Check the code
	
	cout << "Is the " << number << " equal to the encrpted code : " << result << endl;

	robot.setName("Magnus");
	robot.setSurname("Carlsen");
	robot.setAccessState();
	robot.print();

	cout << endl;

	number2 = 1254;
	number = robot.encrypt(number2);
	cout << "The " << number2 << " encrpted code is " << number << endl;

	number2 = number;
	number = robot.decrypt(number2);
	cout << "The " << number2 << " encrpted code is " << number << endl << endl;

	system("pause");
}