#include "Pose.h"
#include <iostream>

using namespace std;

int main()
{
	bool result;
	float number;

	Pose pose1(1, 2, 190);
	Pose pose2(4, 5, 60);
	Pose pose3(7, 8, 90);
	Pose pose4(1, 2, 190);
	Pose pose5;
	
	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;
	cout << "Pose4 : " << pose4.getX() << "," << pose4.getY() << "," << pose4.getTh() << endl;
	
	result = pose1.operator==(pose4);
	cout << "Is the Pose1 and Pose4 equal? : " << result << endl << endl;

	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;
	
	result = pose1.operator==(pose2);
	cout << "Is the Pose1 and Pose2 equal? : " << result << endl << endl;

	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;
	
	pose5 = pose1.operator+(pose2);
	cout << "Pose1 + Pose2 = " << pose5.getX() << "," << pose5.getY() << "," << pose5.getTh() << endl << endl;

	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;
	
	pose5 = pose2.operator-(pose1);
	cout << "Pose2 - Pose1 = " << pose5.getX() << "," << pose5.getY() << "," << pose5.getTh() << endl << endl;

	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;

	pose1 = pose1.operator+=(pose2);
	cout << "Pose1+= Pose2 = " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl << endl;

	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;

	pose1 = pose1.operator-=(pose2);
	cout << "Pose1-= Pose2 = " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl << endl;

	cout << "Pose3 : " << pose3.getX() << "," << pose3.getY() << "," << pose3.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;

	result = pose2.operator<(pose3);
	cout << "Pose2 < Pose3 = " << result << endl << endl;

	cout << "Pose3 : " << pose3.getX() << "," << pose3.getY() << "," << pose3.getTh() << endl;
	cout << "Pose1 : " << pose1.getX() << "," << pose1.getY() << "," << pose1.getTh() << endl;

	result = pose3.operator<(pose1);
	cout << "Pose3 < Pose1 = " << result << endl << endl;

	cout << "Pose3 : " << pose3.getX() << "," << pose3.getY() << "," << pose3.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;

	number = pose2.findDistanceTo(pose3);
	cout << "The distance between Pose2 and Pose3 : " << number << endl << endl;

	cout << "Pose3 : " << pose3.getX() << "," << pose3.getY() << "," << pose3.getTh() << endl;
	cout << "Pose2 : " << pose2.getX() << "," << pose2.getY() << "," << pose2.getTh() << endl;

	number = pose2.findAngleTo(pose3);
	cout << "The angle between Pose2 and Pose3 : " << number << endl << endl;

	system("pause");
}