#pragma once
#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class Record // Record sinifi olusturuldu.
{
public:
	Record();
	bool openFile();   //Yazdirma ve okuma yapilacak dosyayi acar. 
	bool closeFile();  // Yazdirma ve okuma yapilacak dosyayi kapatir. 
	void setFileName(string name);// Yazdirma ve okuma yapilacak dosya adini alir. 
	string readLine();// Dosyadan bir satir veriyi okur. 
	bool writeLine(string str); //Dosyaya bir satir veriyi yazar.
	friend Record& operator<<(Record &record, string str);
	friend Record& operator >> (Record &record, string &str);
	~Record();////Dectructure olarak Record fonksiyonu tanimlandi.
private:
	string fileName;
	fstream file;
};

