/**
 *	@file	:	LaserSensor.h
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Lazer mesafe sensoru icin veri tutma ve yonetimini saglar.
 **/
#pragma once
class LaserSensor
{
public:
	LaserSensor();
	float getRange(int);	///	i. indeksine sahip sensorun mesafe bilgisini dondurur.
	void updateSensor(float[]);	///	Robota ait guncel sensor mesafe degerlerini, 'ranges' dizisine yukler.
	float getMax(int&);	///	Mesafe degerlerinden maksimum olani dondurur.
	float getMin(int&);	///	Mesafe degerlerinden minimum olani dondurur.
	float operator[](int);	///	Indeksi verilen sensor degerini dondurur.
	float getAngle(int);	///	Indeksi verilen sensorun aci degerini dondurur.
	float getClosestRange(float, float, float&);	///	Aci ve mesafeyi dondurur.
	~LaserSensor();	
private:
	float ranges[181];
	//PioneerRobotAPI *robotAPI;
};

