#include <iostream>
#include "Encryption.h"

using namespace std;

int main()
{
	Encryption encryption;
	int result, number = 1254;

	result = encryption.encrypt(number);
	cout << "The " << number << " is encrypted and the result is : " << result << endl << endl;
	
	number = result;
	result = encryption.decrypt(result);
	cout << "The " << number << " is decrypted and the result is : " << result << endl << endl;

	system("pause");
}