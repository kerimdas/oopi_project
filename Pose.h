#pragma once
/*
* @file Pose.h
* @Author Zuhal POLAT (zuhalpolat55@gmail.com)
* @date 14.12.2018
* @brief class Pose
*/

#include <math.h>
#pragma once
class Pose
{
public:
	Pose();														// Constructor without parameters
	Pose(int x, int y, int th);									// Constructor with parameters
	~Pose();													// Deconstructor

	float getX();												// Return x value
	void setX(float x);											// Set x value

	float getY();												// Return y value
	void setY(float y);											// Set y value

	float getTh();												// Return th value
	void setTh(float th);										// Set th value

	bool operator==(const Pose& other);							// Compare if the values are equal
	Pose operator+(const Pose& other);							// Sum of the values
	Pose operator-(const Pose& other);							// Subtract values from each other
	Pose& operator+=(const Pose& other);						// Sum of the values
	Pose& operator-=(const Pose& other);						// Subtractt of the values
	bool operator<(const Pose& other);							// Compare if the values are smaller

	void getPose(float& _x, float& _y, float& _th);				// Get the x, y and th values
	void setPose(float _x, float _y, float _th);				// Set the x, y ad th values

	float findDistanceTo(Pose pos);								// 
	float findAngleTo(Pose pos);								// Find angel between th and pos->th

private:
	float x;
	float y;
	float th;
};