#include <iostream>
#include "Record.h"
#include <fstream>

using namespace std;

int main()
{
	Record record;															//Created a record object
	bool result;									
	string line;
	string lin;
	
	record.setFileName("input.txt");										//Set the file name which one is used in file operations
	
	result = record.openFile();												//Open the file 
	cout << "Is the file opened? : " << result << endl;						//Print the result if the file is opened

	line = record.readLine();												//Get line from the file
	cout << "The line : " << line << endl;									//Print the line 

	line = "Numerical Methods";
	result = record.writeLine(line);										//Write the line("Numerical Methods") to the file
	cout << "Is the "<< line << " writed to the file : " << result << endl;	//Is the line writed to the file

	line = "Object Oriented Programming Lab";
	record << line;															//Write the line to the file with << operator

	record >> lin;															//Get line from the file

	cout << "The line : " << lin << endl;									//Print the line 
		
	record >> lin;															//Get line from the file

	cout << "The line : " << lin << endl;
	record >> lin;															//Get line from the file

	cout << "The line : " << lin << endl;

	result = record.closeFile();											//Close the file
	cout << "Is the file closed? : " << result << endl;						//Print the result if the file is closed
	
	system("pause");
}