/**
 *	@file	:	RobotControl.h
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Robotun simulatordeki kontrolunu saglamak icin olusturulmustur.
 **/

#include <iostream>
using namespace std;

#ifndef ROBOTCONTROL_H_
#define ROBOTCONTROL_H_

#include "PioneerRobotInterface.h"
#include "RobotInterface.h"
#include "Path.h"
#include "Record.h"
#include "RobotOperator.h"
#include "RangeSensor.h"

/**
 *	@brief	:	Robotu simulatorde yonetmek icin olusturulan sinif.
 *
 **/
class RobotControl {
private:
	//Pose* position;
	//PioneerRobotAPI* robotAPI;
	//int state;

	bool key;

	RobotInterface* robotInterface;
	Path path;
	Record record;
	RobotOperator robotOperator;
	RangeSensor rangeSensor;

public:
	void turnLeft();	/// Robotu simulatorde sola dondurur.
	void turnRight();	///	Robotu simulatorde saga dondurur.
	void forward(float speed);	///	Robotu simulatorde ileriye dogru hareket ettirir.
	void safeMoveRobot(float speed);
	void print();	///	Konsola cikti yazdirir.
	void backward(float speed);	///	Robotu simulatorde geriye dogru hareket ettirir.
	Pose getPose();	///	Robotun pozisyonunu dondurur.
	void setPose(Pose);	///	Robotun pozisyonunu ayarlar.
	void stopTurn();	///	Robotun saga veya sola dondurmesini durdurur.
	void stopMove();	///	Robotu durdurur.

	bool addToPath();
	bool clearPath();
	bool recordPathToFile();
	bool openAccess(int);
	bool closeAccess(int);

	RobotControl(PioneerRobotAPI*);	///	Robot sinifinin yapici fonksiyonu.
};

#endif /* ROBOTCONTROL_H_ */