#pragma once
#pragma once
#include <iostream>
#include "Encryption.h"
#include <string>
using namespace std;

class RobotOperator : public Encryption //Encryption'in RobotOperator sinifina inherit edildi.
{
public:
	RobotOperator(int number);
	bool checkAcessCode(int number);// Girilen kodun, sifrelenmis olarak tutulan accessCode ile ayni olup olmadigini kontrol eder.
	void print(); // Operatorun, adini soyadini ve erisim durumunu ekrana getirir.

	void setName(string name); //Operatorun adini ekrana getirir.
	string getName();

	void setSurname(string surname);//Operatorun soyadini ekrana getirir.
	string getSurname();

	void setAccessState();//Operatorun erisim durumunu ekrana getirir.

	~RobotOperator();//Dectructure olarak RobotOperator fonksiyonu tanimlandi.

private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;

	int encryptCode(int number);//4 rakamdan olusan kodu, Encryption sinifinin  fonksiyonu kullanilarak sifrelendirir.
	int decryptCode(int number);//: 4 rakamdan olusan kodu, Encryption sinifinin fonksiyonu kullanilarak sifresini cozdurur. 
};

