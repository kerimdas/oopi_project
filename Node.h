#pragma once
/*
* @file Node.h
* @Author Zuhal POLAT (zuhalpolat55@gmail.com)
* @date 14.12.2018
* @brief class Node
*/

#pragma once
#include "Pose.h"					// Add Pose.h class
class Node
{
public:
	Node *next;
	Pose pose;						// For the node x, y and th values
};

