#include <iostream>
#include "Path.h"

using namespace std;

int main()
{
	int result;
	Pose pose1(1, 2, 3);
	Pose pose2(4, 5, 6);
	Pose pose3(7, 8, 9);

	Path path;

	path.addPose(pose1);
	path.addPose(pose2);
	path.addPose(pose3);

	cout << "The list : " << endl;
	path.print();
	cout << endl;

	cout << "For the new node enter the x, y, th values : ";
	cin >> path;
	cout << endl;

	cout << "The list : " << endl;
	path.print();
	cout << endl;

	result = path.operator[](pose3);
	cout << "The index of the pose3 is : " << result << endl << endl;

	path.removePos(2);
	cout << "The index2 is removed so the list : " << endl;
	path.print();
	cout << endl;

	Pose pose4;
	pose4 = pose2;

	cout << "The new Pose4 is equal to Pose2 and it is inserted to index 2 so the list : " << endl;
	path.insertPos(2, pose3);
	path.print();
	cout << endl;

	system("pause");
}