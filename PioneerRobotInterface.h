/**
 *	@file	:	PioneerRobotInterface.h
 *	@Author	:	kerimdas
 *	@date	:	December, 2018
 *	@brief	:	Robotun simulatordeki kontrolunu saglamak icin olusturulmustur.
 **/

#include "PioneerRobotAPI.h"
#include "RobotInterface.h"

#ifndef PIONEERROBOTINTERFACE_H_
#define PIONEERROBOTINTERFACE_H_

class PioneerRobotInterface : public RobotInterface {
private:
	PioneerRobotAPI* robotAPI;
public:
	void turnLeft();	/// Robotu simulatorde sola dondurur.
	void turnRight();	///	Robotu simulatorde saga dondurur.
	void forward(float speed);	///	Robotu simulatorde ileriye dogru hareket ettirir.
	void safeMoveRobot(float speed);
	void print();	///	Konsola cikti yazdirir.
	void backward(float speed);	///	Robotu simulatorde geriye dogru hareket ettirir.
	Pose getPose();	///	Robotun pozisyonunu dondurur.
	void setPose(Pose);	///	Robotun pozisyonunu ayarlar.
	void stopTurn();	///	Robotun saga veya sola dondurmesini durdurur.
	void stopMove();	///	Robotu durdurur.
	void updateSensors();

	PioneerRobotInterface(PioneerRobotAPI*);
};

#endif /* PIONEERROBOTINTERFACE_H_ */
