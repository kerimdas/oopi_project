#pragma once
class SonarSensor
{
public:
	SonarSensor();
	float getRange(int);
	float getMax(int &index); 
	float getMin(int &index);
	void updateSensor(float[]);
	float operator[](int i);
	float getAngle(int);
	~SonarSensor();
private:
	float ranges[16];
	//PioneerRobotAPI *robotAPI;
};

