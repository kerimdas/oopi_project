/*
* @file Path.h
* @Author Zuhal POLAT (zuhalpolat55@gmail.com)
* @date 14.12.2018
* @brief class Path
*/


#include "Path.h"


// Definition : Path class' default constructor without parameters 
Path::Path()
{
	tail = NULL;
	head = NULL;
	number = 0;
}

// Definition : Path class' deconstructor
Path::~Path()
{
}

// Definition : The function adds a node in linked list
// Parameters : pos in Pose type
void Path::addPose(Pose pos)
{
	Node *temp = new Node;						// Create a Node for the new node
	temp->pose.setX(pos.getX());				// Set the nodes x, y and th values
	temp->pose.setY(pos.getY());
	temp->pose.setTh(pos.getTh());
	temp->next = NULL;							// Set the node next as NULL


	if (head == NULL)							// If the list is empty so the new node is head and tail
	{
		head = temp;
		tail = temp;
	}
	else                                        // If the list is not empty. Add the node at tail
	{
		tail->next = temp;
		tail = temp;
	}
	number++;									// Increment the node number
}

// Definition : The function prints the nodes in linked list
void Path::print()
{
	cout << *this;
}

// Definition : The function is a operator[]. Returns the parameter's values(x, y, th) in linked list
// Parameters : pos in Pose type
int Path::operator[](Pose pos)
{
	Node *p = head;
	int count = 1;

	if (p == NULL)
		return -1;
	else
	{
		while (count <= number)
		{
			if (p->pose == pos)
				return count;
			else
			{
				p = p->next;
				count++;
			}
		}
	}
}

// Definition : The function returns the values(x, y, th) at index in linked list
// Parameters : index in int type
// Return : Returns in Pose type
Pose Path::getPos(int index)
{
	Node *p = head;
	int count = 1;

	if (p == NULL)
		return p->pose;
	else
	{
		while (count <= number)
		{
			if (count == index)
				return p->pose;
			else
			{
				p = p->next;
				count++;

				if (p == NULL)
					break;
			}
		}
	}
}

// Definition : The function removes a node at index in listed link
// Parameters : index in int type
// Return : Returns type is bool. If the node is exist and removed return true. If it is not return false
bool Path::removePos(int index)
{
	Node *p = head;
	int count = 1;

	if (head == NULL)									// List is empty
		return false;
	else
	{
		if (index == 0)									// Index is 0 return head
			head = head->next;
		else
		{
			if (index <= number)						// If index is within reasonable bounds 
			{
				while (count < index - 1)
				{
					p = p->next;
					count++;
				}
				Node *remove = p->next;
				p->next = p->next->next;
				delete remove;
			}
			else
				return false;
		}
		number--;										// Decrement of the number of node
		return true;
	}
}

// Definition : The function insert a node at index in listed link
// Parameters : index in int type, pose in Pose type
// Return : Returns type is bool. If the node is inserted and the index is within reasonable bounds return true. If it is not return false
bool Path::insertPos(int index, Pose pose)
{
	Node *temp = new Node();								// Create a new node for the pose
	temp->next = NULL;
	temp->pose.setX(pose.getX());
	temp->pose.setY(pose.getY());
	temp->pose.setTh(pose.getTh());

	if (index == 1)											// If index is 1 so the new node will be head
	{
		temp->next = head;
		head = temp;
		return true;
	}
	else
	{
		if (index <= number)								// If the index is within reasonable bounds
		{
			Node *p = head;

			for (int i = 0; i < index - 2; i++)
				p = p->next;

			temp->next = p->next;
			p->next = temp;
			return true;
		}
		else
			return false;
	}
}

// Definition : The function is operator<<. It prints the node's x, y and th values
// Parameters : pose in Pose type
ostream & operator<<(ostream & out, Path path)
{
	Node *temp;

	for (temp = path.head; temp != NULL; temp = temp->next)
	{
		out << temp->pose.getX() << "," << temp->pose.getY() << "," << temp->pose.getTh() << endl;
	}

	return out;
}

// Definition : The function is operator>>. 
// Parameters : pose in Pose type
// Return : Returns type is bool. 
istream & operator >> (istream& in, Path path)
{
	float x, y, th;
	in >> x >> y >> th;
	Pose pos(x, y, th);

	path.addPose(pos);

	return in;
}
