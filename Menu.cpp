#include "PioneerRobotAPI.h"
#include "RobotControl.h"
#include <iostream>
#include <string>
#include <conio.h>
using namespace std;


PioneerRobotAPI *robot;
float sonars[16];
float laserData[181];

void print() {
	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	cout << "Sonar ranges are [ ";
	robot->getSonarRange(sonars);
	for (int i = 0; i < 16; i++) {
		cout << sonars[i] << " ";
	}
	cout << "]" << endl;
	cout << "Laser ranges are [ ";
	robot->getLaserRange(laserData);
	for (int i = 0; i < 181; i++) {
		cout << laserData[i] << " ";
	}
	cout << "]" << endl;
}

int main()
{
	int choice;
	
	robot = new PioneerRobotAPI;
	RobotControl *robotControl;
	robotControl = new RobotControl(robot);

	while (1)
	{
		system("cls");

		cout << "Main Menu" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Sensor" << endl;
		cout << "4. Quit" << endl;
		
		cout << endl << "Choice one : ";
		cin >> choice;
		
		switch (choice)
		{
		case 1:
			system("cls");
			cout << "Connection Menu" << endl;
			cout << "1. Connect Robot" << endl;
			cout << "2. Disconnect Robot" << endl;
			cout << "3. Back" << endl;

			cout << endl << "Choice one : ";
			cin >> choice;

			switch (choice)
			{
			case 1:
				cout << endl << "<Connect>" << endl;
				
				if (robot->connect())
					cout << "Robot is connected..." << endl;
				else
					cout << "Robot is not connected..." << endl;
				
				break;
			case 2:
				cout << endl << "<Disconnect>" << endl;

				if (robot->connect())
				{
					cout << "Robot is disconnected..." << endl;
					exit(0);
				}

				else
					cout << "Robot is not disconnected..." << endl;

				break;
			case 3:
				break;
			}
			break;
		case 2:
			system("cls");
			cout << "Motion Menu" << endl;
			cout << "1. Move Robot" << endl;
			cout << "2. Safe Move Robot" << endl;
			cout << "3. Turn Left" << endl;
			cout << "4. Turn Right" << endl;
			cout << "5. Forward" << endl;
			cout << "6. Move Distance" << endl;
			cout << "7. Close Wall" << endl;
			cout << "8. Quit" << endl;

			cout << "Choice one : ";
			cin >> choice;

			switch (choice)
			{
			case 1:
				robot->moveRobot(100);
				print();
				break;
			case 2:
				//robotControl->safeMoveRobot(100, robot);
				break;
			case 3:
				robotControl->turnLeft();
				break;
			case 4:
				robotControl->turnRight();
				break;
			case 5:
				robotControl->forward(100);
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				exit(0);
			}
			break;
		case 3:
			system("cls");
			cout << "Sensor Menu" << endl;
			cout << "1. Move Robot" << endl;
			cout << "2. Safe Move Robot" << endl;
			cout << "3. Turn Left" << endl;
			cout << "4. Turn Right" << endl;
			cout << "5. Forward" << endl;
			cout << "6. Move Distance" << endl;
			cout << "7. Close Wall" << endl;
			cout << "8. Quit" << endl;

			cout << "Choice one : ";
			cin >> choice;

			switch (choice)
			{
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				exit(0);
			}
			break;
		case 4:
			exit(0);
		default:
			cout << "Wrong choice..." << endl << endl;
			break;
		}
		cout << "Press Enter to Continue";
		_getch();
	}

	system("pause");
}