/*
* @file Path.h
* @Author Zuhal POLAT (zuhalpolat55@gmail.com)
* @date 14.12.2018
* @brief class Path
*/

#pragma once
#include "Pose.h"								// Add the Pose.h class
#include "Node.h"								// Add the Node.h class
#include <iostream>

using namespace std;

class Path :public Pose							// Inherit the Pose class in Path class
{
public:
	Path();										// Default constructor
	~Path();									// Default deconstructor

	void addPose(Pose pos);						// Add node in linked list
	void print();								// Print the linked list nodes
	int operator[](Pose pos);					// Return pos index in linked list 
	Pose getPos(int index);						// Return index values 
	bool removePos(int index);					// Remove node from linked list
	bool insertPos(int index, Pose pose);		// Insert a node in where the user wanted
	friend ostream & operator<<(ostream& out, const Path path);					// Print the node x, y, th values
	friend istream & operator >> (istream& in, Path pose);					// Get the nodes x, y and th values

private:
	Node * tail;								// Tail of the linked list
	Node* head;									// Head of the linked list
	int number;									// Number of the node number
};