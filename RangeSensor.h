#pragma once
#include "PioneerRobotAPI.h"

class RangeSensor
{
public:
	RangeSensor();
	~RangeSensor();

	virtual float getRange(int index);
	virtual float getMax(int& index);
	virtual float getMin(int& index);
	virtual void updateSensor(float ranges[16]);
	float operator[](int i);
	virtual float getAngle(int index);
	virtual float getClosestRange(float startAngle, float endAngle, float& angle);


private:
	float ranges[16];
	PioneerRobotAPI* robotAPI;
};
