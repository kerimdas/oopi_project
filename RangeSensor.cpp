#include "RangeSensor.h"



RangeSensor::RangeSensor()
{
	for (int i = 0; i < 16; i++)
	{
		ranges[i] = 0;
	}
}


RangeSensor::~RangeSensor()
{
}

float RangeSensor::getRange(int index)
{
	return ranges[index];
}

float RangeSensor::getMax(int & index)
{
	float max = ranges[0];

	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] > max)
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}

float RangeSensor::getMin(int & index)
{
	float min = ranges[0];

	for (int i = 0; i < 16; i++)
	{
		if (ranges[i] < min)
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

void RangeSensor::updateSensor(float ranges[16])
{
	for (int i = 0; i < 16; i++)
	{
		this->ranges[i] = ranges[i];
	}
}

float RangeSensor::operator[](int i)
{
	return ranges[i];
}

float RangeSensor::getAngle(int index)
{
	return ranges[index];
}

float RangeSensor::getClosestRange(float startAngle, float endAngle, float & angle)
{
	float closest = ranges[(int)startAngle];

	for (int i = startAngle + 1; i < endAngle; i++)
	{
		if (closest > ranges[i])
		{
			closest = ranges[i];
			angle = i;
		}
	}
	return closest;
}
